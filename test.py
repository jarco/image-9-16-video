# Create a ZOOM-PAN effect using OpenCV and Python. Am image is read and the
# effect can be created either about the centre of the image or about any 
# specified point. This code is equivalent to FFmpeg commands for zoom-in and
# zoom-out effect described below. This effect is also called Ken Burns effect.

#Start with original dimension and zoom-in 0.0015 every iteration
#ffmpeg -loop 1 -i image.png -vf "zoompan=z='min(zoom+0.0015,1.5)':d=125" -c:v libx264 -t 5 -s "800x450" -y zoomedIn.mp4

#Start zoomed in at 1.5 and zoom out 0.0015 every iteration
#ffmpeg -loop 1 -i image.png -vf "zoompan= z='if(lte(zoom,1.0),1.5, max(1.001, zoom-0.0015))':d=125" -c:v libx264 -t 5 -s "800x450" -y zoomedout.mp4

import cv2
from pathlib import Path
import glob
import os

nfs = 40        #Number of Frames per Second, video duration = nframe/nfs
nframe = 800    #Total number of frames in the output vidoe
max_zoom = 1.7  #Maximum Zoom level (should be > 1.0)
max_rot = 0    #Maximum rotation in degrees, set '0' for no rotation

# Get all png and jpg files from the current directory
image_files = glob.glob("*.png") + glob.glob("*.jpg") + glob.glob("*.jpeg") + glob.glob("*.webp")




#function of image to movie
def image2video(imgName, vidName, nfs, nframe, max_zoom, max_rot):
	def zoomPan(img, zoom=1, angle=0, coord=None):
		cy, cx = [i/2 for i in img.shape[:-1]] if coord is None else coord[::-1]
		rot = cv2.getRotationMatrix2D((cx, cy), angle, zoom)
		res = cv2.warpAffine(img, rot, img.shape[1::-1], flags=cv2.INTER_LINEAR)
		return res

	img = cv2.imread(imgName)
	w = img.shape[1]
	h = img.shape[0]
	codec = cv2.VideoWriter_fourcc(*'mp4v')  #other formats can be DIVX or DIVD
	video = cv2.VideoWriter(vidName, codec, nfs, (w, h))

	#Make the loop for Zooming-in
	i = 1
	while i < nframe:
		zLvl = 1.0 + i / (1/(max_zoom-1)) / nframe
		angle = i * max_rot / nframe
		zoomedImg = zoomPan(img, zLvl, angle, coord=None)
		video.write(zoomedImg)
		i = i + 1

	#Make the loop for Zooming-out to starting position
	# i = nframe
	# while i > 0:
	# 	zLvl = 1.0 + i / (1/(max_zoom-1)) / nframe
	# 	angle = i * max_rot / nframe
	# 	zoomedImg = zoomPan(img, zLvl, angle, coord=None)
	# 	video.write(zoomedImg)
	# 	i = i - 1
		
	video.release()

def convertTo916(vidName, name):
    input_video = cv2.VideoCapture(vidName)
    width = int(input_video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(input_video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Calculate the new dimensions for the 9:16 aspect ratio
    new_width = width
    new_height = int(width * 16 / 9)

    if height < new_height:
        pad_top = (new_height - height) // 2
        pad_bottom = new_height - height - pad_top
    else:
        return False

    output_video = cv2.VideoWriter(name + '_916.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 30, (new_width, new_height))

    while input_video.isOpened():
        ret, frame = input_video.read()
        if not ret:
            break
        # Create a black image with the new dimensions
        padded_frame = cv2.copyMakeBorder(frame, pad_top, pad_bottom, 0, 0, cv2.BORDER_CONSTANT, value=(0, 0, 0))
        output_video.write(padded_frame)

    output_video.release()
    return True

# check if output directoy exists, if not, create it. 
if not os.path.exists("output"):
	os.makedirs("output")

for image in image_files:
	#get filename without extention
	name = Path(image).stem
	imgName = image
	vidName = name + ".mp4"
	image2video(imgName, vidName, nfs, nframe, max_zoom, max_rot)
	converted = convertTo916(vidName, name)
	#remove the original video if it was converted and move the converted video, else move the first video
	if converted:
		os.rename(name + "_916.mp4", "output/" + name + "_916.mp4")
		os.remove(vidName)
	else:
		os.rename(vidName, "output/" + vidName)